package java;

public class Order {
    
    private Long id;
    private String name;
    private Integer amount;
    private Integer discount;

    public Log getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getDiscount() {
        return discount;        
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

}