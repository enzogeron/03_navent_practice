package java;

// Importar BumexMemcached y pedidosDAO ya implementadas
import BumexMemcached;
import PedidosDAO;

import Order;

public class ServiceOrder extends Order {

    PedidosDAO pedidosDAO;
    BumexMemcached bumexMemcached;

    public ServiceOrder() {
        super();
    }

    public void createOrder(Order order) {
        pedidosDAO.insertOrUpdate(order);
        bumexMemcached.set(id.toString(), order);
        System.out.println('Se creo un nuevo producto.');
    }

    public void updateOrder(Long id) {
        Order order = getOrder(id);
        pedidosDAO.insertOrUpdate(order);
        bumexMemcached.set(id.toString(), order);
        System.out.println('Se actualizo el producto.');
    }

    public Order getOrder(Long id) {
        Order order = (Order) bumexMemcached.get(id.toString());
        if(order != null) {
            return order;
        }
        order = pedidosDAO.select(id);
        if(order != null) {
            return order;
        }
        System.out.println('El pedido no esta disponible.');
    }

    public void deleteOrder(Order order) {
        pedidosDAO.delete(order.getId().toString());
        bumexMemcached.delete(order);
        System.out.println('Se elimino el producto.');
    }

}