const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const orders = require('./routes/order');
const app = express();

const port = process.env.PORT || 8000;
const db = 'mongodb://mongo/navent';

mongoose.connect(db, { useNewUrlParser: true }, (err) => {
    if(err) {
        console.log('Error en la conexion a la DB');
    } else {
        console.log('Se ha establecido la conexion con la DB');
    }
});

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use('/', orders);

// Handle errors HTTP 500, HTTP 404
app.use((req, res, next) => {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    if(err.status == 404) {
        res.status(404).json({ message: 'Not found' });
    } else {
        res.status(500).json({ message: 'Error server!' });
    }
});

app.listen(port, () => {
    console.log('Server ON');
});

