const orderModel = require("../models/order");

module.exports = {
    create: (req, res, next) => {
        
        let order = {
            name: req.body.name,
            amount: req.body.amount,
            discount: req.body.discount
        }

        orderModel.create(order, (err, result) => {
            if(err) {
                res.json({
                    status: 'error',
                    message: 'No fue posible crear el pedido.'
                });
            } else {
                res.status(200).json({
                    status: 'success',
                    message: 'El pedido se ha guardado correctamente.'
                });
            }
        });

    }
}