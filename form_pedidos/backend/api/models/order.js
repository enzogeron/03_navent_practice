const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    name: {
        type: String,
        maxlength: 100,
        trim: true,
        required: true
    },
    amount: {
        type: Number,
        required: true

    },
    discount: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Order', OrderSchema);