const express = require('express');
const router = express.Router();

const ordersController = require('../api/controllers/orders');

router.post('/pedidos/guardar', ordersController.create);

module.exports = router;