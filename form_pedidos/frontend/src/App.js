import { useState } from 'react';

const App = () => {

  let initialState = {
    name: '',
    amount: '',
    discount: ''
  }

  const [data, setData] = useState(initialState);
  const [errors, setErrors] = useState(initialState);

  const api = 'http://localhost:8081/pedidos/guardar';

  const validateForm = () => {
    let fields = data;
    let errors = {};
    let formValid = true;

    if (!fields.name) {
      formValid = false;
      errors.name = "*Ingrese su nombre.";
      if(fields.name.length > 100) {
        errors.name = "*Solo se permiten 100 caracteres.";
      }
    }

    if(!fields.amount || !(parseInt(fields.amount) > 0)) {
      formValid = false;
      errors.amount = "*Ingrese un monto valido.";
    }

    if(!fields.discount || !(parseInt(fields.discount) > 0)) {
      formValid = false;
      errors.discount = "*Ingrese un descuento valido.";
    }

    setErrors(errors);
    return formValid;

  }

  const handleSubmit = (e) => {
    e.preventDefault();

    if(validateForm()) {
      
      const requestOptions = {
        method: 'POST',
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      };

      fetch(api, requestOptions)
        .then(async response => {
            const data = await response.json();

            if (!response.ok) {
                const error = (data && data.message) || response.status;
                return Promise.reject(error);
            }

            alert(data.message);
            setData({ ...initialState });

        })
        .catch(error => {
            alert(error);
        });

    }
  }

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  }

  return (
    <div className="col-6 mx-auto">
      <form className="card card-body" onSubmit={ handleSubmit }>
        <div className="form-group">
          <label htmlFor="name">Nombre</label>
          <input
            type="text"
            name="name"
            className="form-control"
            maxLength="100"
            onChange={ handleChange }
            value={ data.name }
          />
          <small className="text-danger">{ errors.name }</small>
        </div>
        <div className="form-group">
          <label htmlFor="amount">Monto</label>
          <input 
            type="number"
            name="amount"
            className="form-control"
            onChange={ handleChange }
            value={ data.amount }
          />
          <small className="text-danger">{ errors.amount }</small>
        </div>
        <div className="form-group">
          <label htmlFor="discount">Descuento</label>
          <input
            type="number"
            name="discount"
            className="form-control"
            min="0"
            onChange={ handleChange }
            value={ data.discount }
          />
          <small className="text-danger">{ errors.discount }</small>
        </div>
        <button className="btn btn-primary btn-block">
          Enviar
        </button>
      </form>
    </div>
  );
}

export default App;